import java.util.*;

public class Node {


    private String name;
    private Node firstChild;
    private Node nextSibling;

    Node (String n, Node d, Node r) {
        this.name = n;
        this.firstChild = d;
        this.nextSibling = r;
    }


    public static Node parsePostfix (String s) {
        checkForErrors(s);
        Stack<Node> stack = new Stack<>();
        Node node = new Node(null,null,null);
        String[] str = s.split("");
        boolean root = false;
        for (int i = 0; i < str.length; i++) {
            String token = str[i].trim();
            switch (token) {
                case "(":
                    stack.push(node);
                    node.firstChild = new Node(null, null, null);
                    node = node.firstChild;
                    if (str[i + 1].trim().equals(",")) {
                        throw new RuntimeException("Comma must not come after node " + s);
                    }
                    break;
                case ")":
                    node = stack.pop();
                    if (stack.empty()) {
                        root = true;
                    }
                    break;
                case ",":
                    if (root) {
                       throw new RuntimeException("Parenthesis can't be followed by comma " + s);
                    }
                    node.nextSibling = new Node(null, null, null);
                    node = node.nextSibling;
                    break;
                default:
                    if (node.name == null) {
                        node.name = token;
                    } else {
                        node.name += token;
                    }
                    break;
            }
        }
        return node;

    }

    public String leftParentheticRepresentation() {
        StringBuilder str = new StringBuilder();
        str.append(name);
        if (firstChild != null) {
            str.append("(");
            str.append(firstChild.leftParentheticRepresentation());
            str.append(")");
        }
        if (nextSibling != null) {
            str.append(",");
            str.append(nextSibling.leftParentheticRepresentation());
        }
        return str.toString();
    }

    public static void checkForErrors(String s){
        //Checking if there is any case of an invalid input string in the parsePostfix method
        if (s.contains("()")) {
            throw new RuntimeException("Node name must be non-empty");
        } else if (s.contains("((") && s.contains("))")) {
            throw new RuntimeException("Node must not contain round brackets" + s);
        } else if (s.contains(",,")) {
            throw new RuntimeException("Node must not contain double commas" + s);
        } else if (s.contains("\t")) {
            throw new RuntimeException("Node must not contain whitespace symbols" + s);
        } else if (s.contains(" ")) {
            throw new RuntimeException("Node must not contain whitespaces" + s);
        } else if (s.contains(",") && !(s.contains("(") && s.contains(")"))) {
            throw new RuntimeException("Invalid input " + s + " must contain brackets (example: (B1,C,D)A ) ");
        }
    }



    public static void main (String[] param) {
        String s = "(B1,C)A";
        Node t = Node.parsePostfix (s);
        String v = t.leftParentheticRepresentation();
        System.out.println (s + " ==> " + v); // (B1,C)A ==> A(B1,C)
    }

}
//https://git.wut.ee/i231/home5/src/commit/e98832db0e1292bb6bb952aab1905e9db1aa5fd2